# Invitations

## Invite someone to join a group

To invite accounts to your group, you have to:

  1. click the **My groups** button on the top bar menu
  * click the group you want to invite people to in the list
  * click the **Add / Remove…** link in your group banner
  * enter the new member ID in the **Invite a new member** field (e.g: `someone@instance.org`)
  * click the **Invite member** button

The invited account will receive an email notification if [notifications are activated](../../users/account-settings/#email-notifications).

!!! note
    By default, the invited account's role is **member**.


## Being invited to join a group

When someone invites you to join a group (see above), you will receive an email notification.

From the **My groups** page, you either **accept** or **decline** this invitation.

!!! note
    By default your role is **member**.

![goup invitation](../../images/en/group-accept-invitation-EN.png)

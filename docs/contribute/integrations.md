# Integrations

Mobilizon provides [a GraphQL API](./graphql_api.md) and can be integrated with other platforms or tools.

## Libraries

Nothing here yet.

## Tools

* [mobilizon-poster](https://github.com/vecna/mobilizon-poster)  
  A simple nodejs script(s) to interact with mobilizon via command line
* [mobilizon-reshare](https://github.com/Tech-Workers-Coalition-Italia/mobilizon-reshare)
  Set of tools to publish Mobilizon events to external plaforms (Telegram, Facebook, Mastodon, Zulip, Twitter currently supported)
* [Mobilizon Mirror](https://wordpress.org/plugins/mobilizon-mirror/)
  Wordpress plugin that syncs events inside custom post type
* [Mobilizon Block](https://codeberg.org/linos/mobilizon-block)
  Wordpress Plugin. Integrates nicely in existing theme. Show events of specific group or whole instance. Use a Gutenberg-Block (not a shortcode). Fetches the events on the server side (php), not on the client side (JavaScript).

## CMS

* [Wordpress plugin](https://wordpress.org/plugins/connector-mobilizon/) ([Source on Github](https://github.com/dwaxweiler/connector-mobilizon))
* [Jekyll plugin](https://github.com/Marc-AntoineA/jekyll-mobilizon)

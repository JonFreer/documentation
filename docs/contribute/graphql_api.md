# GraphQL API

Mobilizon features a [GraphQL API](https://graphql.org). Authentication is handled with [Json Web Tokens](https://jwt.io). Each instance provides a GraphQL playground at `/graphiql`, such as https://mobilizon.fr/graphiql that you can use to explore the schema. You can also use [the schema's representation](https://framagit.org/framasoft/mobilizon/-/blob/main/schema.graphql).

Each instance's endpoint is at `/api`.

## Examples

# List events

Let's list a few public events of the instance

```graphql
query {
  events {
    elements {
      id,
      url,
      title,
      description,
      beginsOn,
      endsOn,
      status,
      picture {
        url
      },
      physicalAddress {
        id,
        description,
        locality
      }
    }
    total
  }
}
```

# Authenticate

Follow the [API auth section](api_auth.md) to know how to create an application and authenticate.

Once an accessToken is provided, you must include it in your calls to perform authenticated calls, through the `Authorization: Bearer $token` HTTP header.

# Create an event

```graphql
mutation createEvent(
    $organizerActorId: ID!,
    $title: String!,
    $description: String!,
    $beginsOn: DateTime!,
    $endsOn: DateTime,
    $status: EventStatus,
    $visibility: EventVisibility,
  ) {
    createEvent(
      organizerActorId: $organizerActorId,
      title: $title,
      description: $description,
      beginsOn: $beginsOn,
      endsOn: $endsOn,
      status: $status,
      visibility: $visibility,
    ) {
      id,
      uuid,
      title,
      url,
      local,
      description,
      beginsOn,
      endsOn,
      status,
      visibility,
      joinOptions,
      draft
    }
}
```
